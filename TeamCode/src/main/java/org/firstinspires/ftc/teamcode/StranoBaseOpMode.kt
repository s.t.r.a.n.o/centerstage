package org.firstinspires.ftc.teamcode

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode
import com.qualcomm.robotcore.hardware.DcMotor

abstract class StranoBaseOpMode : LinearOpMode() {

    companion object {
        const val REV_HD_HEX_MOTOR_MAX_VELOCITY = 2800
    }

    private lateinit var frontLeftDrive: DcMotor
    private lateinit var frontRightDrive: DcMotor
    private lateinit var backLeftDrive: DcMotor
    private lateinit var backRightDrive: DcMotor

    /**
     * Move robot forward/backward, strafing left/right or turning CC/CCW
     * @param drive +1.0 drive forward, -1.0 move backward
     * @param strafe +1.0 strafe left, -1.0 strafe right
     * @param turn +1.0 turn counter-clockwise, -1.0 turn clockwise
     */
    fun moveRobot(drive: Double, strafe: Double, turn: Double) {

    }

    override fun runOpMode() {
        telemetry.addData("Status", "Base OpMode initialized")
        telemetry.update()
    }
}