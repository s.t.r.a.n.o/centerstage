package org.firstinspires.ftc.teamcode

import com.qualcomm.robotcore.eventloop.opmode.Autonomous
import com.qualcomm.robotcore.eventloop.opmode.Disabled
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode

@Autonomous(name = "S.T.R.A.N.O. Autonomous", preselectTeleOp = "S.T.R.A.N.O. TeleOp")
//@Disabled
class StranoAuto : LinearOpMode() {

    private lateinit var robotDrivetrain: RobotDrivetrain
    private lateinit var robotVision: RobotVision
    override fun runOpMode() {
        robotDrivetrain = RobotDrivetrain(this, true)
        robotVision = RobotVision(this)
        while (opModeInInit()) {
            telemetry.addData("Status", "Touch PLAY to run autonomous mode")
            robotDrivetrain.readSensors()
            telemetry.update()
        }
        // Reset heading for autonomous drive mode
        robotDrivetrain.resetHeading()
        if (opModeIsActive()) {
            robotDrivetrain.drive(50.0, 0.5, 0.25) // positive => forward
            robotVision.resumeStream()
            while (opModeIsActive()) {
                telemetry.update()
                robotVision.telemetryTfod()
                sleep(20)
            }
            //robotDrivetrain.turnTo(90.0, 0.45, 0.5) // positive => CCW
            //robotDrivetrain.strafe(-100.0, 0.5, 0.25) // positive => right
        }
        robotVision.close()
    }
}