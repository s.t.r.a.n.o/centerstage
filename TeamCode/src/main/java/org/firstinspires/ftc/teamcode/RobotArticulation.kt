/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.firstinspires.ftc.teamcode

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode
import com.qualcomm.robotcore.hardware.DcMotor
import com.qualcomm.robotcore.hardware.DcMotorSimple
import com.qualcomm.robotcore.hardware.PwmControl
import com.qualcomm.robotcore.hardware.Servo
import com.qualcomm.robotcore.hardware.ServoImplEx
import org.firstinspires.ftc.robotcore.external.BlocksOpModeCompanion.gamepad1
import kotlin.math.abs

class RobotArticulation(
    private val opMode:LinearOpMode
) {
    companion object {
        const val MOTOR_REV_CORE_HEX_ENCODER_COUNTS_PER_REV = 288
        const val MOTOR_ARM_GEAR_RATIO = 15 / 90                      //_Plastic Hex Bore Gears
    }

    private val downLeftLift = opMode.hardwareMap.get(DcMotor::class.java, "downLeftLift")
    private val downRightLift = opMode.hardwareMap.get(DcMotor::class.java, "downRightLift")
    private var isLiftRising = false
    private var liftPosition = 0
    private val rightArm = opMode.hardwareMap.get(DcMotor::class.java, "rightArm")
    private val leftArm = opMode.hardwareMap.get(DcMotor::class.java, "leftArm")
    private val rightArmPositionUp = 1100 // TODO: calculate value from encoder and gear ratio
    private val leftArmPositionUp = 1100
    private val rightArmPositionDown = 0
    private val leftArmPositionDown = 0
    private var armAngle = 0.0
    private val globalPower = 0.5
    private val ePos0 = 0.225
    private val ePos1 = 0.5

    private val droneLauncher = opMode.hardwareMap.get(ServoImplEx::class.java, "drone_launcher")
    private val eRotation1 = opMode.hardwareMap.get(ServoImplEx::class.java, "eRotation1")
    private val eRotation2 = opMode.hardwareMap.get(ServoImplEx::class.java, "eRotation2")
    private val eGrab = opMode.hardwareMap.get(ServoImplEx::class.java, "eGrab")


    init {
        // TODO: qua impostare motori e direzione
        downLeftLift.direction = DcMotorSimple.Direction.FORWARD
        downRightLift.direction = DcMotorSimple.Direction.REVERSE
        liftPosition = downRightLift.currentPosition
        initArm()
        droneLauncher.pwmRange = PwmControl.PwmRange(500.0, 2500.0)
        eRotation1.pwmRange = PwmControl.PwmRange(500.0, 2500.0)
        eRotation2.pwmRange = PwmControl.PwmRange(500.0, 2500.0)
        eGrab.pwmRange = PwmControl.PwmRange(500.0, 2500.0)
    }

    fun raiseLift(power: Double) {
        isLiftRising = power > 0.5
        val currentPosition = downRightLift.currentPosition
        if (isLiftRising) {
            if (currentPosition == liftPosition) {
                downLeftLift.power = 0.1
                downRightLift.power = 0.1
            } else {
                val power = power - 0.4
                downLeftLift.power = power
                downRightLift.power = power
            }
        } else {
            // Lift is falling
            if (abs(currentPosition - liftPosition) < 2) { // abs(currentPosition - liftPosition) < 2
                downLeftLift.power = -0.1
                downRightLift.power = -0.1
            } else {
                val power = power - 0.6
                downLeftLift.power = power
                downRightLift.power = power
            }
        }
        liftPosition = currentPosition
        opMode.telemetry.addData("power", "%.2f %.2f", downLeftLift.power, downRightLift.power)
    }

    fun launchDrone() {
        droneLauncher.position = 0.65
        //waitForStart()
        //runtime.reset()
    }

    fun moveGripper() {
        eRotation1.position = ePos1
        eRotation2.position = ePos0
        //waitForStart()
        //runtime.reset()

        eGrab.position = 0.3
        //waitForStart()
        //runtime.reset()
    }

    fun initArm() {
        leftArm.direction = DcMotorSimple.Direction.FORWARD
        rightArm.direction = DcMotorSimple.Direction.REVERSE
        leftArm.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER)
        rightArm.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER)
        leftArm.setTargetPosition(leftArmPositionDown)
        rightArm.setTargetPosition(rightArmPositionDown)
        leftArm.setMode(DcMotor.RunMode.RUN_TO_POSITION)
        rightArm.setMode(DcMotor.RunMode.RUN_TO_POSITION)
        getArmAngle()
    }

    fun getArmAngle(): Double {
        armAngle =
            (360 * MOTOR_ARM_GEAR_RATIO * leftArm.currentPosition).toDouble() / MOTOR_REV_CORE_HEX_ENCODER_COUNTS_PER_REV
        return armAngle
    }

    fun moveArm(y: Boolean, dpad_left: Boolean, a: Boolean, dpad_down: Boolean) {
        if (y == false && dpad_left == false && a == false && dpad_down == false) {
            leftArm.setTargetPosition(rightArmPositionDown)
            rightArm.setTargetPosition(rightArmPositionDown)
            leftArm.setMode(DcMotor.RunMode.RUN_TO_POSITION)
            rightArm.setMode(DcMotor.RunMode.RUN_TO_POSITION)
            leftArm.power = globalPower
            rightArm.power = globalPower
        } else if (y == true && dpad_left == false && a == false && dpad_down == false) {
            leftArm.setTargetPosition(rightArmPositionUp - 200)
            rightArm.setTargetPosition(rightArmPositionUp - 200)
            leftArm.setMode(DcMotor.RunMode.RUN_TO_POSITION)
            rightArm.setMode(DcMotor.RunMode.RUN_TO_POSITION)
            leftArm.power = globalPower
            rightArm.power = globalPower
        } else if (y == false && dpad_left == false && dpad_down == true) {
            leftArm.setTargetPosition(rightArmPositionUp)
            rightArm.setTargetPosition(rightArmPositionUp)
            leftArm.setMode(DcMotor.RunMode.RUN_TO_POSITION)
            rightArm.setMode(DcMotor.RunMode.RUN_TO_POSITION)
            leftArm.power = globalPower
            rightArm.power = globalPower
        } else if (y == false && dpad_left == true && dpad_down == false) {
            leftArm.setTargetPosition(rightArmPositionUp - 900)
            rightArm.setTargetPosition(rightArmPositionUp - 900)
            leftArm.setMode(DcMotor.RunMode.RUN_TO_POSITION)
            rightArm.setMode(DcMotor.RunMode.RUN_TO_POSITION)
            leftArm.power = globalPower
            rightArm.power = globalPower
            //eRotation1.position = 0.3625
            //eRotation2.position = 0.3625
        }

        opMode.telemetry.addData("arm pos y left a down", "%b %b %b %b", y, dpad_left, a, dpad_down)
        opMode.telemetry.addData("arm", "%d %.2f", leftArm.currentPosition, getArmAngle())

    }

    fun launchDrone(active: Float) {
        if (active > 0.5) {
            droneLauncher.position = 1.0
        } else {
            droneLauncher.position = 0.65
        }
    }

    fun grabPixel(active: Boolean) {
        if (active) {
            eGrab.position = 0.4
        } else {
            eGrab.position = 1.0
        }
    }

    fun RotatePixel1(a: Boolean, b: Boolean) {
        if (a == false && b == true) {
            eRotation1.position = ePos0
            eRotation2.position = ePos1
        } else if (a == true && b == false) {
            eRotation1.position = ePos1
            eRotation2.position = ePos0
        } else if (a == false && b == false) {
            val init = 0.05
            eRotation1.position = init
            eRotation2.position = 1.0 - init
        }


        /*

    downLeftLift.velocity = liftdown.toDouble()
    downRightLift.velocity = liftdown.toDouble()
     */
    }
}