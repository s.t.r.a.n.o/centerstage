package org.firstinspires.ftc.teamcode

import com.qualcomm.hardware.lynx.LynxModule
import com.qualcomm.hardware.lynx.LynxModule.BulkCachingMode
import com.qualcomm.hardware.rev.RevHubOrientationOnRobot
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode
import com.qualcomm.robotcore.hardware.DcMotorEx
import com.qualcomm.robotcore.hardware.DcMotorSimple
import com.qualcomm.robotcore.hardware.IMU
import com.qualcomm.robotcore.util.ElapsedTime
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit
import kotlin.math.PI
import kotlin.math.abs

/*
 * Inspiration taken from "Mr. Phil's Simplified Odometry Approach" video series
 * See also:
 *  - https://www.youtube.com/playlist?list=PLKxIFjlI_T1wDKiNfjf5xtMbpqU4TLCkG
 *  - https://github.com/gearsincorg/SimplifiedOdometry
 */
class RobotDrivetrain(
    private val opMode: LinearOpMode,
    private val showTelemetry: Boolean = false
) {

    companion object {

        // Drive motor & encoder constants
        const val MOTOR_REV_HD_HEX_ENCODER_COUNTS_PER_REV = 28 // REV HD Hex Motor REV-41-1298
        const val MOTOR_REV_HD_HEX_FREE_RUNNING_RPM = 6000     // 6000rpm -> 300rpm
        const val MOTOR_REV_HD_HEX_GEARBOX_RATIO = 20          // 20:1
        const val MOTOR_DRIVE_COUNTS_PER_REVOLUTION =
            MOTOR_REV_HD_HEX_ENCODER_COUNTS_PER_REV * MOTOR_REV_HD_HEX_GEARBOX_RATIO // 560 cts/rev
        const val DRIVE_REV_OMNI_WHEEL_DIAMETER_IN_MM = 90     // REV Omni Wheel 90mm REV-41-1190
        const val DRIVE_WHEEL_CIRCUMFERENCE_IN_CM = PI * DRIVE_REV_OMNI_WHEEL_DIAMETER_IN_MM / 10
        const val DRIVE_ODOMETRY_CORRECTION = 1.21 //1.427755568246716 // Empirically measured
        const val DRIVE_ODOMETRY_CM_PER_COUNT = DRIVE_ODOMETRY_CORRECTION *
            DRIVE_WHEEL_CIRCUMFERENCE_IN_CM / MOTOR_DRIVE_COUNTS_PER_REVOLUTION
        const val DRIVE_VELOCITY_COUNTS_PER_SEC =
            MOTOR_REV_HD_HEX_FREE_RUNNING_RPM / MOTOR_REV_HD_HEX_GEARBOX_RATIO / 60 * MOTOR_DRIVE_COUNTS_PER_REVOLUTION

        // Drive proportional control settings
        const val DRIVE_GAIN = 0.03
        const val DRIVE_ACCELERATION = 2.0
        const val DRIVE_TOLERANCE = 0.5
        const val DRIVE_DEADBAND = 0.2
        const val DRIVE_MAX_AUTO = 0.6

        // Strafe proportional control settings
        const val STRAFE_GAIN = 0.03
        const val STRAFE_ACCELERATION = 1.5
        const val STRAFE_TOLERANCE = 0.5
        const val STRAFE_DEADBAND = 0.2
        const val STRAFE_MAX_AUTO = 0.6

        // Yaw proportional control settings
        const val YAW_GAIN = 0.018       // Strength of the yaw position control.
        const val YAW_ACCELERATION = 3.0 // Acceleration limit in degrees per second.
        const val YAW_TOLERANCE = 1.0    // No action is required if less than this +/- amount.
        const val YAW_DEADBAND = 0.25    // Error less than this with 0 output. Must be < tolerance.
        const val YAW_MAX_AUTO = 0.6     // Default maximum yaw movement limit.
    }

    private val driveFrontLeft = opMode.hardwareMap.get(DcMotorEx::class.java, "frontLeftDrive")
    private val driveFrontRight = opMode.hardwareMap.get(DcMotorEx::class.java, "frontRightDrive")
    private val driveBackLeft = opMode.hardwareMap.get(DcMotorEx::class.java, "backLeftDrive")
    private val driveBackRight = opMode.hardwareMap.get(DcMotorEx::class.java, "backRightDrive")
    private val imu = opMode.hardwareMap.get(IMU::class.java, "imu")
    private val holdTimer = ElapsedTime()
    private var heading = 0.0f       // Latest robot heading from IMU in degrees
    private var rawHeading = 0.0    // Unmodified heading in degrees
    private var headingOffset = 0.0 // Set offset heading in degrees
    private var turnRate = 0.0f     // Latest robot turn rate from IMU in degrees per second
    private var rawOdometryDrive = 0.0
    private var rawOdometryStrafe = 0.0
    private var distanceDrive_cm = 0.0f
    private var distanceStrafe_cm = 0.0f
    private var odometerOffsetDrive = 0.0f
    private var odometerOffsetStrafe = 0.0f

    public val driveController = ProportionalControl(
        DRIVE_GAIN,
        DRIVE_ACCELERATION,
        DRIVE_MAX_AUTO,
        DRIVE_TOLERANCE,
        DRIVE_DEADBAND,
        false
    )

    public val strafeController = ProportionalControl(
        STRAFE_GAIN,
        STRAFE_ACCELERATION,
        STRAFE_MAX_AUTO,
        STRAFE_TOLERANCE,
        STRAFE_DEADBAND,
        false
        )

    public val yawController = ProportionalControl(
        YAW_GAIN,
        YAW_ACCELERATION,
        YAW_MAX_AUTO,
        YAW_TOLERANCE,
        YAW_DEADBAND,
        true
    )

    init {
        driveFrontLeft.direction = DcMotorSimple.Direction.FORWARD
        driveFrontRight.direction = DcMotorSimple.Direction.REVERSE
        driveBackLeft.direction = DcMotorSimple.Direction.FORWARD
        driveBackRight.direction = DcMotorSimple.Direction.REVERSE
        // TODO: Use fake encoders or odometry wheels for x,y?
        // Set all hubs to AUTO bulk caching mode for faster encoder reads
        val hubs = opMode.hardwareMap.getAll(LynxModule::class.java)
        for (hub in hubs) {
            hub.bulkCachingMode = BulkCachingMode.AUTO
        }
        // Control Hub orientation on robot drivetrain
        val imuOrientation = RevHubOrientationOnRobot(
            RevHubOrientationOnRobot.LogoFacingDirection.DOWN,
            RevHubOrientationOnRobot.UsbFacingDirection.RIGHT
        )
        imu.initialize(IMU.Parameters(imuOrientation))
        resetOdometry()
    }

    private fun readSensorsOdometry() {
        // Compute fake drive axis from all drive wheels
        rawOdometryDrive = listOf(
            driveFrontLeft.currentPosition, driveFrontRight.currentPosition,
            driveBackLeft.currentPosition, driveBackRight.currentPosition
        ).average()
        distanceDrive_cm =
            (DRIVE_ODOMETRY_CM_PER_COUNT * (rawOdometryDrive - odometerOffsetDrive)).toFloat()
        // Compute fake strafe axis from all drive wheels
        rawOdometryStrafe = listOf(
            driveFrontLeft.currentPosition, -driveFrontRight.currentPosition,
            -driveBackLeft.currentPosition, driveBackRight.currentPosition
        ).average()
        distanceStrafe_cm =
            (DRIVE_ODOMETRY_CM_PER_COUNT * (rawOdometryStrafe - odometerOffsetStrafe)).toFloat()
    }

    fun readSensors(): Boolean {
        // TODO: Get x,y odometry encoder values
        readSensorsOdometry()
        val robotAngles = imu.robotYawPitchRollAngles
        val robotAngularVelocity = imu.getRobotAngularVelocity(AngleUnit.DEGREES)
        rawHeading = robotAngles.getYaw(AngleUnit.DEGREES)
        heading = (rawHeading - headingOffset).toFloat()
        turnRate = robotAngularVelocity.zRotationRate
        if (showTelemetry) {
            // TODO: Add x,y odometry telemetry
            opMode.telemetry.addData("Odom. [DR. ST.]", "[%.1fcnts %.1fcnts]",
                rawOdometryDrive - odometerOffsetDrive, rawOdometryStrafe - odometerOffsetStrafe)
            opMode.telemetry.addData("Dist. [DR. ST.]", "[%.2fcm %.2fcm]",
                distanceDrive_cm, distanceStrafe_cm)
            opMode.telemetry.addData("Head. [Deg. Rate]", "[%.2f %.2f]", heading, turnRate)
        }
        return true
    }

    fun resetOdometry() {
        readSensors()
        odometerOffsetDrive = rawOdometryDrive.toFloat()
        distanceDrive_cm = 0.0f
        driveController.reset(0.0)
        odometerOffsetStrafe = rawOdometryStrafe.toFloat()
        distanceStrafe_cm = 0.0f
        strafeController.reset(0.0)
    }

    fun resetHeading() {
        readSensors()
        headingOffset = rawHeading
        yawController.reset(0.0)
        heading = 0.0f
    }

    fun getHeading(): Float {
        return heading.toFloat()
    }

    fun getTurnRate(): Float {
        return turnRate
    }

    fun moveRobot(drive: Float, strafe: Float, yaw: Float) {

        var powerFrontLeft = drive + strafe - yaw
        var powerFrontRight = drive - strafe + yaw
        var powerBackLeft = drive - strafe - yaw
        var powerBackRight = drive + strafe + yaw
        val powerMax = listOf(
            abs(powerFrontLeft),
            abs(powerFrontRight),
            abs(powerBackLeft),
            abs(powerBackRight)
        ).max()
        if (powerMax > 1.0) {
            powerFrontLeft /= powerMax
            powerFrontRight /= powerMax
            powerBackLeft /= powerMax
            powerBackRight /= powerMax
        }

        driveFrontLeft.velocity = powerFrontLeft.toDouble() * DRIVE_VELOCITY_COUNTS_PER_SEC
        driveFrontRight.velocity = powerFrontRight.toDouble() * DRIVE_VELOCITY_COUNTS_PER_SEC
        driveBackLeft.velocity = powerBackLeft.toDouble() * DRIVE_VELOCITY_COUNTS_PER_SEC
        driveBackRight.velocity = powerBackRight.toDouble() * DRIVE_VELOCITY_COUNTS_PER_SEC
        if (showTelemetry) {
            opMode.telemetry.addData("Axes [DR. ST. Yaw]", "[%.2f%% %.2f%% %.2f%%]", drive, strafe, yaw)
            /*opMode.telemetry.addData("Drive p. [FL. FR. BL. BR.]", "[%.2f %.2f %.2f %.2f]",
                powerFrontLeft, powerFrontRight, powerBackLeft, powerBackRight
            )*/
            opMode.telemetry.addData(
                "Drive v. [FL. FR. BL. BR.]", "[%.1fc/s %.1fc/s %.1fc/s %.1fc/s]",
                driveFrontLeft.velocity, driveFrontRight.velocity,
                driveBackLeft.velocity, driveBackRight.velocity
            )
        }
    }

    // funzioni drive, turnTo, strafe
    fun drive(distance_cm: Double, power: Double, holdTime: Double) {
        resetOdometry()
        driveController.reset(distance_cm, power)
        strafeController.reset(0.0)
        yawController.reset()
        holdTimer.reset()

        while (opMode.opModeIsActive() && readSensors()) {
            moveRobot(
                driveController.getOutput(distanceDrive_cm),
                strafeController.getOutput(distanceStrafe_cm),
                yawController.getOutput(heading)
            )
            if (driveController.isInPosition() && yawController.isInPosition()) {
                if (holdTimer.time() > holdTime) {
                    break // arrived at destination
                }
            } else {
                holdTimer.reset()
            }
            opMode.sleep(10) // loop for 10 milliseconds
        }
        stopRobot()
    }



    fun strafe(distance_cm: Double, power: Double, holdTime: Double) {
        resetOdometry()
        driveController.reset(0.0)
        strafeController.reset(distance_cm, power)
        yawController.reset()
        holdTimer.reset()

        while (opMode.opModeIsActive() && readSensors()) {
            moveRobot(
                driveController.getOutput(distanceDrive_cm),
                strafeController.getOutput(distanceStrafe_cm),
                yawController.getOutput(heading)
            )
            if(strafeController.isInPosition() && yawController.isInPosition()) {
                if(holdTimer.time() > holdTime) {
                    break; //Is at destination
                }
            } else {
                holdTimer.reset()
            }
            opMode.sleep(10);
        }
        stopRobot()
    }


    fun turnTo(headingDeg: Double, power: Double, holdTime: Double) {
        yawController.reset(headingDeg, power)
        while (opMode.opModeIsActive() && readSensors()) {
            moveRobot(0f,0f, yawController.getOutput(heading))
            if (yawController.isInPosition()) {
                if(holdTimer.time() > holdTime) {
                    break //At position
                }
            }
            else {
                holdTimer.reset();
            }
            opMode.sleep(10)
        }
        stopRobot()
    }

    fun stopRobot(){
        moveRobot(0f,0f,0f)
    }

}