package org.firstinspires.ftc.teamcode

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode
import com.qualcomm.robotcore.hardware.DcMotorSimple
import com.qualcomm.robotcore.hardware.PwmControl
import org.firstinspires.ftc.robotcontroller.external.samples.StranoAutoPixel
import org.firstinspires.ftc.robotcore.external.hardware.camera.BuiltinCameraDirection
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName
import org.firstinspires.ftc.vision.VisionPortal
import org.firstinspires.ftc.vision.tfod.TfodProcessor

class RobotVision (private val opMode: LinearOpMode) {
    private var tfod: TfodProcessor? = null
    private var visionPortal: VisionPortal? = null

    companion object {
        const val USE_WEBCAM = true // true for webcam, false for phone camera
        const val CAMERA_WIDTH = 640
        const val CAMERA_HEIGHT = 480
    }
    init {
        // Create the TensorFlow processor the easy way.
        tfod = TfodProcessor.easyCreateWithDefaults()

        // Create the vision portal the easy way.
        visionPortal = if (USE_WEBCAM) {
            VisionPortal.easyCreateWithDefaults(
                opMode.hardwareMap.get(WebcamName::class.java, "Webcam 1"), tfod
            )
        } else {
            VisionPortal.easyCreateWithDefaults(
                BuiltinCameraDirection.BACK, tfod
            )
        }
    }

        fun telemetryTfod() {
        val currentRecognitions = tfod!!.recognitions
        opMode.telemetry.addData("# Objects Detected", currentRecognitions.size)

        // Step through the list of recognitions and display info for each one.
        for (recognition in currentRecognitions) {
            val x = ((recognition.left + recognition.right) / 2).toDouble()
            val y = ((recognition.top + recognition.bottom) / 2).toDouble()
            opMode.telemetry.addData("", " ")
            opMode.telemetry.addData(
                "Image",
                "%s (%.0f %% Conf.)",
                recognition.label,
                recognition.confidence * 100
            )
            opMode.telemetry.addData("- Position", "%.0f / %.0f", x, y)
            opMode.telemetry.addData("- Size", "%.0f x %.0f", recognition.width, recognition.height)
            // TODO: LEFT, CENTER, RIGHT

        } // end for() loop
    } // end method telemetryTfod()

    fun close() {
        visionPortal!!.close()
    }

    fun resumeStream() {
        visionPortal!!.resumeStreaming()
    }
}