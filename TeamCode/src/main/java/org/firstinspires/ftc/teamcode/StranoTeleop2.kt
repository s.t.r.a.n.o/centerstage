/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.firstinspires.ftc.teamcode

import com.qualcomm.robotcore.eventloop.opmode.Disabled
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode
import com.qualcomm.robotcore.eventloop.opmode.TeleOp
import com.qualcomm.robotcore.hardware.DcMotor
import com.qualcomm.robotcore.hardware.DcMotorEx
import com.qualcomm.robotcore.hardware.DcMotorSimple
import com.qualcomm.robotcore.hardware.Servo
import com.qualcomm.robotcore.util.ElapsedTime
import com.qualcomm.robotcore.util.Range

/*
 * This file contains an minimal example of a Linear "OpMode". An OpMode is a 'program' that runs in either
 * the autonomous or the teleop period of an FTC match. The names of OpModes appear on the menu
 * of the FTC Driver Station. When a selection is made from the menu, the corresponding OpMode
 * class is instantiated on the Robot Controller and executed.
 *
 * This particular OpMode just executes a basic Tank Drive Teleop for a two wheeled robot
 * It includes all the skeletal structure that all linear OpModes contain.
 *
 * Use Android Studio to Copy this Class, and Paste it into your team's code folder with a new name.
 * Remove or comment out the @Disabled line to add this OpMode to the Driver Station OpMode list
 */
@TeleOp(name = "Basic: Linear OpMode Kotlin 2", group = "Linear OpMode")
@Disabled
class StranoTeleop2 : LinearOpMode() {

    // Declare OpMode members.
    private val runtime = ElapsedTime()
    private lateinit var droneLauncher: Servo
    private lateinit var eRotation: Servo
    private lateinit var eGrab: Servo

    private lateinit var frontLeftDrive: DcMotorEx
    private lateinit var frontRightDrive: DcMotorEx
    private lateinit var backLeftDrive: DcMotorEx
    private lateinit var backRightDrive: DcMotorEx



    override fun runOpMode() {
        telemetry.addData("Status", "Initialized")
        telemetry.update()

        // Initialize the hardware variables. Note that the strings used here as parameters
        // step (using the FTC Robot Controller app on the phone).
        droneLauncher = hardwareMap.get(Servo::class.java, "drone_launcher")
        eRotation = hardwareMap.get(Servo::class.java, "eRotation")
        eGrab = hardwareMap.get(Servo::class.java, "eGrab")
        // to 'get' must correspond to the names assigned during the robot configuration

        frontLeftDrive = hardwareMap.get(DcMotorEx::class.java, "frontLeftDrive")
        frontRightDrive = hardwareMap.get(DcMotorEx::class.java, "frontRightDrive")
        backLeftDrive = hardwareMap.get(DcMotorEx::class.java, "backLeftDrive")
        backRightDrive = hardwareMap.get(DcMotorEx::class.java, "backRightDrive")




        // To drive forward, most robots need the motor on one side to be reversed, because the axles point in opposite directions.
        // Pushing the left stick forward MUST make robot go forward. So adjust these two lines based on your first test drive.
        // Note: The settings here assume direct drive on left and right wheels.  Gear Reduction or 90 Deg drives may require direction flips
        frontLeftDrive.direction = DcMotorSimple.Direction.REVERSE
        frontRightDrive.direction = DcMotorSimple.Direction.FORWARD
        backLeftDrive.direction = DcMotorSimple.Direction.REVERSE
        backRightDrive.direction = DcMotorSimple.Direction.FORWARD





        // Wait for the game to start (driver presses PLAY)
        droneLauncher.position=1.0
        waitForStart()
        runtime.reset()

        eRotation.position=0.25
        waitForStart()
        runtime.reset()

        eGrab.position=1.0
        waitForStart()
        runtime.reset()

        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {

            // Setup a variable for each drive wheel to save power level for telemetry
            var leftPower: Float
            var rightPower: Float

            // Choose to drive using either Tank Mode, or POV Mode
            // Comment out the method that's not used.  The default below is POV.

            // POV Mode uses left stick to go forward, and right stick to turn.
            // - This uses basic math to combine motions and is easier to drive straight.
            val drive = gamepad1.left_stick_y
            val turn = -gamepad1.right_stick_x / 2


            val x = gamepad1.left_stick_x
            val y = gamepad1.left_stick_y
            val yaw = gamepad1.right_stick_x / 2

            leftPower = Range.clip(drive + turn, -1.0f, 1.0f)
            rightPower = Range.clip(drive - turn, -1.0f, 1.0f)

            // Calculate wheel powers.

            // Calculate wheel powers.
            val frontLeft = -x + y - yaw
            val rightFront = x + y + yaw
            val leftBack = x + y - yaw
            val rightBack = -x + y + yaw

            // Tank Mode uses one stick to control each wheel.
            // - This requires no math, but it is hard to drive forward slowly and keep straight.
            leftPower  = -gamepad1.left_stick_y ;
            rightPower = -gamepad1.right_stick_y ;

            // Send calculated power to wheels
            frontLeftDrive.velocity = frontLeft.toDouble()
            frontRightDrive.velocity = rightFront.toDouble()
            backLeftDrive.velocity = leftBack.toDouble()
            backRightDrive.velocity = rightBack.toDouble()

            if(gamepad1.b) {
                droneLauncher.position=1.0
            }
            else{
                droneLauncher.position=0.65
            }

            if(gamepad1.a) {
                eRotation.position=0.0
            }
            else{
                eRotation.position=0.25
            }

            if(gamepad1.x) {
                eGrab.position=0.6
            }
            else{
                eGrab.position=1.0   
            }

            // Show the elapsed game time and wheel power.
            telemetry.addData("Status", "Run Time: $runtime")
            telemetry.addData("Motors", "left (%.2f), right (%.2f)", leftPower, rightPower)
            telemetry.update()
            
        }
    }
}
