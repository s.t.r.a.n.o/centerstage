package org.firstinspires.ftc.teamcode

import android.util.Size
import com.qualcomm.robotcore.eventloop.opmode.Disabled
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode
import com.qualcomm.robotcore.eventloop.opmode.TeleOp
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName
import org.firstinspires.ftc.vision.VisionPortal
import org.firstinspires.ftc.vision.apriltag.AprilTagProcessor

@TeleOp(name = "Strano Vision")
@Disabled
class StranoAprilTag : LinearOpMode() {

    override fun runOpMode() {
        val aprilTagProcessor = AprilTagProcessor.Builder()
            .setDrawAxes(true)
            .setDrawCubeProjection(true)
            .setDrawTagID(true)
            .setDrawTagOutline(true)
            .build()

        val visionPortal = VisionPortal.Builder()
            .addProcessor(aprilTagProcessor)
            .setCamera(hardwareMap.get(WebcamName::class.java, "Webcam 1"))
            .setCameraResolution(Size(640, 480))
            .build()

        waitForStart()
        while (opModeIsActive()) {
            for (tag in aprilTagProcessor.detections) {
                val pose = tag.ftcPose
                telemetry.addLine(String.format("%d XYZ %6.2f %6.2f %6.2f", tag.id, pose.x, pose.y, pose.z))
                telemetry.addLine(String.format("%d RPY %6.2f %6.2f %6.2f", tag.id, pose.roll, pose.pitch, pose.yaw))

                // See also:
                // https://ftc-docs.firstinspires.org/en/latest/apriltag/vision_portal/apriltag_intro/apriltag-intro.html#apriltag-pose
                // https://www.youtube.com/watch?v=CjoXoygzXzI
                // https://www.youtube.com/watch?v=IYFpXJ6-_WY
            }
            telemetry.update()
        }
    }
}