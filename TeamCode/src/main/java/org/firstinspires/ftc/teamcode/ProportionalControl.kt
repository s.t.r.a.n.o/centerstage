package org.firstinspires.ftc.teamcode

import com.qualcomm.robotcore.util.ElapsedTime
import com.qualcomm.robotcore.util.Range
import kotlin.math.abs

class ProportionalControl(
    private val gain: Double,
    private val accelerationLimit: Double,
    private val outputLimit: Double,
    private val tolerance: Double,
    private val deadband: Double,
    private val circular: Boolean
) {

    private var liveOutputLimit = 0.0
    private var setPoint = 0.0
    private var lastOutput = 0.0
    private var inPosition = false
    private val cycleTime = ElapsedTime()

    init {
        reset(0.0)
    }

    fun getOutput(input: Float): Float {
        var error = setPoint - input
        val dV = cycleTime.seconds() * accelerationLimit
        var output: Double
        // Normalize to +/- 180 as given by IMU if we are controlling heading
        if (circular) {
            while (error > 180) { error -= 360 }
            while (error <= -180) { error += 360 }
        }
        // Set flag when the error is below the tolerance value
        inPosition = abs(error) < tolerance
        // Prevent any slow accumulated motor output
        if (abs(error) <= deadband) {
            output = 0.0
        } else {
            // Calculate output power using gain and clip it to the limits
            output = error * gain
            output = Range.clip(output, -liveOutputLimit, liveOutputLimit)
            // Limit also rate of change (acceleration)
            if ((output - lastOutput) > dV) {
                output = lastOutput + dV
            } else if ((output - lastOutput) < -dV) {
                output = lastOutput - dV
            }
        }
        lastOutput = output
        cycleTime.reset()
        return output.toFloat()
    }

    fun isInPosition(): Boolean {
        return inPosition
    }

    fun reset(setPoint: Double, powerLimit: Double) {
        liveOutputLimit = abs(powerLimit)
        this.setPoint = setPoint
        reset()
    }

    fun reset(setPoint: Double) {
        reset(setPoint, outputLimit)
    }

    fun reset() {
        cycleTime.reset()
        inPosition = false
        lastOutput = 0.0
    }
}