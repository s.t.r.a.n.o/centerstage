/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.firstinspires.ftc.teamcode

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode
import com.qualcomm.robotcore.eventloop.opmode.TeleOp
import com.qualcomm.robotcore.hardware.DcMotor
import com.qualcomm.robotcore.hardware.Servo
import com.qualcomm.robotcore.util.ElapsedTime
import kotlin.math.abs

/*
 * This file contains an minimal example of a Linear "OpMode". An OpMode is a 'program' that runs in either
 * the autonomous or the teleop period of an FTC match. The names of OpModes appear on the menu
 * of the FTC Driver Station. When a selection is made from the menu, the corresponding OpMode
 * class is instantiated on the Robot Controller and executed.
 *
 * This particular OpMode just executes a basic Tank Drive Teleop for a two wheeled robot
 * It includes all the skeletal structure that all linear OpModes contain.
 *
 * Use Android Studio to Copy this Class, and Paste it into your team's code folder with a new name.
 * Remove or comment out the @Disabled line to add this OpMode to the Driver Station OpMode list
 */
@TeleOp(name = "S.T.R.A.N.O. TeleOp")
//@Disabled
class StranoTeleop3 : LinearOpMode() {

    // Declare OpMode members.
    private val runtime = ElapsedTime()
    private lateinit var droneLauncher: Servo
    private lateinit var eRotation: Servo
    private lateinit var eGrab: Servo

    private lateinit var downLeftLift: DcMotor
    private lateinit var downRightLift: DcMotor

    private lateinit var robotDrivetrain: RobotDrivetrain
    private lateinit var robotArticulation: RobotArticulation
    private var autoHeading = false


    override fun runOpMode() {
        robotDrivetrain = RobotDrivetrain(this, true)
        robotArticulation = RobotArticulation(this)

        while (opModeInInit()) {
            telemetry.addData("Status", "Touch PLAY to drive")
            if (!reset()) {
                robotDrivetrain.readSensors()
                robotDrivetrain.resetHeading()
                robotDrivetrain.resetOdometry()
                //robotArticulation.moveArm()
            }
            telemetry.update()
        }

        while (opModeIsActive()) {
            telemetry.addData("Status", "Robot is active")
            if (!reset()) {
                robotDrivetrain.readSensors()

            }
            drive()
            robotArticulation.raiseLift(gamepad1.right_trigger.toDouble())
            robotArticulation.moveArm(gamepad1.dpad_up, gamepad1.dpad_left, gamepad1.dpad_down, gamepad1.dpad_down)
            robotArticulation.launchDrone(gamepad1.left_trigger)
            robotArticulation.grabPixel(gamepad1.x)
            robotArticulation.RotatePixel1(gamepad1.a, gamepad1.b)
            telemetry.update()
        }

        /*return
        telemetry.addData("Status", "Initialized")
        telemetry.update()

        // Initialize the hardware variables. Note that the strings used here as parameters
        // to 'get' must correspond to the names assigned during the robot configuration
        // step (using the FTC Robot Controller app on the phone).
        droneLauncher = hardwareMap.get(Servo::class.java, "drone_launcher")
        eRotation = hardwareMap.get(Servo::class.java, "eRotation")
        eGrab = hardwareMap.get(Servo::class.java, "eGrab")

        frontLeftDrive = hardwareMap.get(DcMotor::class.java, "frontLeftDrive")
        frontRightDrive = hardwareMap.get(DcMotor::class.java, "frontRightDrive")
        backLeftDrive = hardwareMap.get(DcMotor::class.java, "backLeftDrive")
        backRightDrive = hardwareMap.get(DcMotor::class.java, "backRightDrive")

        //downLeftLift = hardwareMap.get(DcMotor::class.java, "downLeftLift")
        // = hardwareMap.get(DcMotor::class.java, "downRightLift")

        // To drive forward, most robots need the motor on one side to be reversed, because the axles point in opposite directions.
        // Pushing the left stick forward MUST make robot go forward. So adjust these two lines based on your first test drive.
        // Note: The settings here assume direct drive on left and right wheels.  Gear Reduction or 90 Deg drives may require direction flips
        frontLeftDrive.direction = DcMotorSimple.Direction.REVERSE
        frontRightDrive.direction = DcMotorSimple.Direction.FORWARD
        backLeftDrive.direction = DcMotorSimple.Direction.REVERSE
        backRightDrive.direction = DcMotorSimple.Direction.FORWARD

        // Wait for the game to start (driver presses PLAY)
        droneLauncher.position=1.0
        waitForStart()
        runtime.reset()

        eRotation.position=0.25
        waitForStart()
        runtime.reset()

        eGrab.position=1.0
        waitForStart()
        runtime.reset()

        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {

            // Setup a variable for each drive wheel to save power level for telemetry
            var leftPower: Float
            var rightPower: Float

            // Choose to drive using either Tank Mode, or POV Mode
            // Comment out the method that's not used.  The default below is POV.

            // POV Mode uses left stick to go forward, and right stick to turn.
            // - This uses basic math to combine motions and is easier to drive straight.
            val drive = gamepad1.left_stick_y
            val turn = -gamepad1.right_stick_x / 2

            //val liftdown = gamepad1.left_trigger
            //val liftup = gamepad1.right_trigger

            val x = gamepad1.left_stick_x
            val y = gamepad1.left_stick_y
            val yaw = gamepad1.right_stick_x / 2

            leftPower = Range.clip(drive + turn, -1.0f, 1.0f)
            rightPower = Range.clip(drive - turn, -1.0f, 1.0f)

            // Calculate wheel powers.

            // Calculate wheel powers.
            val frontLeft = -x + y - yaw
            val rightFront = x + y + yaw
            val leftBack = x + y - yaw
            val rightBack = -x + y + yaw

            // Tank Mode uses one stick to control each wheel.
            // - This requires no math, but it is hard to drive forward slowly and keep straight.
            // leftPower  = -gamepad1.left_stick_y ;
            // rightPower = -gamepad1.right_stick_y ;

            // Send calculated power to wheels
            frontLeftDrive.power = frontLeft.toDouble()
            frontRightDrive.power = rightFront.toDouble()
            backLeftDrive.power = leftBack.toDouble()
            backRightDrive.power = rightBack.toDouble()

            if(gamepad1.b) {
                droneLauncher.position=1.0
            }
            else{
                droneLauncher.position=0.65
            }

            if(gamepad1.a) {
                eRotation.position=0.0
            }
            else{
                eRotation.position=0.25
            }

            if(gamepad1.x) {
                eGrab.position=0.6
            }
            else{
                eGrab.position=1.0   
            }

            // Show the elapsed game time and wheel power.
            telemetry.addData("Status", "Run Time: $runtime")
            telemetry.addData("Motors", "left (%.2f), right (%.2f)", leftPower, rightPower)
            telemetry.update()
        }*/
    }

    private fun reset(): Boolean {
        // Reset gyro when both BACK and START is pressed on the Logitech F310 gamepad
        if (gamepad1.left_bumper) { //&& gamepad1.right_bumper) {
            telemetry.addData("Gyro", "Reset")
            robotDrivetrain.resetHeading()
            robotDrivetrain.resetOdometry()
            return true
        }
        return false
    }

    private fun drive() {
        val drive = -gamepad1.left_stick_y // F310 axis is inverted TODO: Add safe drive speed factor
        val strafe = gamepad1.left_stick_x // F310 axis is correct TODO: Add safe strafe speed factor
        var yaw = -gamepad1.right_stick_x / 2 // F310 axis is inverted TODO: Add safe yaw speed factor
        // TODO: Add DPAD orthogonal motions with less speed and more precision
        // Switch heading if needed
        if (abs(yaw) > 0.05) {
            autoHeading = false
        } else {
            // If heading not locked wait until robot finished rotating (< 2 deg per second)
            if (!autoHeading && abs(robotDrivetrain.getTurnRate()) < 2.0) {
                // TODO: set robot drivetrain yaw PID controller to current heading
                autoHeading = true
            }
        }
        if (autoHeading) {
            // TODO: get yaw PID controller output from current robot heading
            val oldYaw = yaw
            yaw = robotDrivetrain.yawController.getOutput(robotDrivetrain.getHeading())
            telemetry.addData("Auto head. enabled [Deg. Deg.]", "[%.2f %.2f]", oldYaw, yaw)
        } else {
            telemetry.addData("Auto head. disabled [Deg.]", "[%.2f]", yaw)
        }
        // TODO: continue on line 93 from SampleTeleop.java
        robotDrivetrain.moveRobot(drive, strafe, yaw)
    }
}
